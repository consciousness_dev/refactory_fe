let example = [4,9,7,5,8,9,3]

function sortSwapCount(arr) {
  let ln = arr.length;
  let swapEl;
  let counter = 0;
  do {
    swapEl = false;
    let n = 0;
    do {
      while(arr[n] > arr[n+1]) {
        let temp = arr[n];
        arr[n] = arr[n+1];
        arr[n+1] = temp;
        counter++;
        console.log(`${counter}.[${arr[n]},${arr[n+1]}] -> ${arr.join(" ")}`);
        swapEl = true;
      }
      arr[n] < arr[n-1] ? n=0 : n++;
    }while(n < ln)
  } while(swapEl);
  console.log(`\nJumlah swap: ${counter}`);
  return arr;
}

sortSwapCount(example);