import './App.css';
import React, { useState } from 'react';
function App() {

  let timer = 0;
  let delayClk = 200;
  let prevent = false;

  const [value, setValue] = useState({
    inputText: '',
    outputText: '',
    textArr: [],
    textIndex:0,
  });

  const handleChange = (elName) => {
    setValue( value=>({
      ...value,
      [elName.target.name]: elName.target.value,
      textArr: [...value.textArr, elName.target.value]
    }));
  }

  const reverseText = () => {
    setValue({
      ...value,
      outputText: value.inputText.split("").reverse().join("")
    });
  }

  const handleClick = () => {
    timer = setTimeout(()=>{
      if(!prevent) {
        undoAction();
      }
      prevent = false;
    }, delayClk);
  }

  const handleDblClick = () => {
    clearTimeout(timer);
    prevent = true;
    redoAction();
  }

  const undoAction = () => {
    if(value.textIndex === 0) {
      return;
    }
    value.textIndex -=1;
    setValue(value=>({
      ...value,
      inputText: value.textArr[value.textIndex]
    }))
  }

  const redoAction = () => {
    if(value.textIndex === value.textArr.length-1) {
      return;
    }

    value.textIndex +=1;
    setValue(value=>({
      ...value,
      inputText: value.textArr[value.textIndex]
    }))
  }

  return (
    <div className="App">
      <div className="main__wrapper">
        <div className="main__input-text">
          <input name="inputText" type="text" onChange={handleChange} value={value.inputText} />
        </div>
        <div className="main__output-text">
          <h1>Output: {value.outputText}</h1>
        </div>
        <div className="main__button-action">
          <button className="button button__reverse" onClick={reverseText}>Reverse</button>
          <button className="button button__unredo" onClick={handleClick} onDoubleClick={handleDblClick}>Undo/Redo</button>
        </div>
      </div>
    </div>
  );
}

export default App;
